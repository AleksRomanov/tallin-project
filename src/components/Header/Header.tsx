import React from 'react';
import {HeaderStyles} from './Header.styles.tw';
import Image from '../../assets/img/banner-desktop.jpg'

function Header() {
  return (
    <HeaderStyles className="header">
      <img className={'header-background'} src={Image} alt=""/>
    </HeaderStyles>
  );
}

export default Header;
