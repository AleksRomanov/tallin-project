import tw from 'twin.macro';
import styled from 'styled-components';

export const GalleryStyles = styled.div`
  ${tw`relative mb-[] pb-[]`}

  .gallery-nav {
    display: block;
    margin: 0 auto;
    ${tw``}
  }
  .gallery-swiper {
    ${tw`mb-[124px] pl-[20px] lg:pl-[128px]`}
  }

  .gallery-list {
    position: relative;
  }

  .gallery-list-item {
    ${tw`h-[284px] lg:w-[313px] lg:h-[336px]`}
  }

  .gallery-button-prev {
    ${tw`absolute bottom-[-64px] left-[132px] lg:left-[664px]`}
    border-radius: 40px;
    height: 40px;
    width: 40px;
    border: 1px solid #EAEAEA;
    &:hover {
      background-color: #E7717D;
    }
    
    svg {
      fill: #EAEAEA;
      height: 12px;
      width: 12px;
      top: 12px;
      left: 12px;
      position: absolute;
    }

    &:hover .arrow {
      fill: #ffffff;
    }
  }
}

.gallery-button-next {
  ${tw`absolute bottom-[-64px] left-[192px] lg:left-[736px]`}
  border-radius: 40px;
  height: 40px;
  width: 40px;
  border: 1px solid #E7717D;

  &:hover {
    background-color: #E7717D;
  }

  svg {
    height: 12px;
    width: 12px;
    top: 13px;
    left: 14px;
    position: absolute;
  }

  &:hover .arrow {
    fill: #ffffff;
  }
}
`
