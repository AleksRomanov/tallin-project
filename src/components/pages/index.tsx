import React from 'react';
import Teaser from '../Teaser/Teaser';
import Gallery from '../gallery/Gallery';
import Tours from '../Tours/Tours';

function Index(): JSX.Element {
  return (
    <>
      <Teaser/>
      <Gallery/>
      <Tours/>
    </>
  );
}

export default Index;
