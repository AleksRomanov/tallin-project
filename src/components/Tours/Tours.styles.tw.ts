import tw from 'twin.macro';
import styled from 'styled-components';

export const ToursStyles = styled.div`
  ${tw`relative mb-[128px] pl-[20px] lg:pl-[128px] text-black`}
  h1 {
    ${tw`mr-[auto] lg:mr-[0] text-[24px] mb-[40px] lg:mb-[64px] lg:text-[40px] lg:order-1`}
  }

  .tours-list-item {
    ${tw`lg:w-[373px] lg:h-[566px] lg:mr-[33px] lg:block lg:p-[30px] lg:pb-[200px]`}
    &__image {
      ${tw`mb-[24px]`}
    }
    &__title {
      ${tw`text-[20px] mb-[16px]`}
    }
    &__description {
      ${tw`text-[14px]`}
    }
  }
  .tours-button-prev {
    ${tw`absolute bottom-[-64px] left-[132px] lg:hidden `}
    border-radius: 40px;
    height: 40px;
    width: 40px;
    border: 1px solid #EAEAEA;
    &:hover {
      background-color: #E7717D;
    }

    svg {
      fill: #EAEAEA;
      height: 12px;
      width: 12px;
      top: 12px;
      left: 12px;
      position: absolute;
    }

    &:hover .arrow {
      fill: #ffffff;
    }
  }
}

.tours-button-next {
  ${tw`absolute bottom-[-64px] left-[192px] lg:hidden`}
  border-radius: 40px;
  height: 40px;
  width: 40px;
  border: 1px solid #E7717D;

  &:hover {
    background-color: #E7717D;
  }

  svg {
    height: 12px;
    width: 12px;
    top: 13px;
    left: 14px;
    position: absolute;
  }

  &:hover .arrow {
    fill: #ffffff;
  }
}
`
