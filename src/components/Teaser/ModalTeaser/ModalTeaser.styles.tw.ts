import tw from 'twin.macro';
import styled from 'styled-components';

export const ModalTeaserStyles = styled.div`
  ${tw`relative h-[1455px]  lg:m-[0 auto] lg:px-[40px] lg:py-[64px] lg:w-[1184px]`}
  h1 {
    ${tw`text-[24px] mb-[46px]`}
  }

  .modal-teaser-content {
    ${tw`lg:flex lg:flex-wrap`}
    p {
      margin-bottom: 16px;
      color: rgba(35, 35, 35, .7);
    }

    &__input {
      ${tw`lg:h-[64px]`}
      border-radius: 0;
      margin-bottom: 24px;
    }

    .modal-input-wrapper {
      ${tw`lg:w-[536px] lg:mr-[15px]`}
      &__default {
        ${tw`lg:hidden`}
      }
      &__mail {
        ${tw``}
      }
      .input-select {
        ${tw`lg:w-[536px] lg:h-[64px]`}
        border-radius: 0;
        //width: 335px;
        margin-bottom: 40px;
      }
    }
    &__buttons {
      ${tw`lg:w-[100%] lg:flex lg:justify-center lg:mb-[64px]`}
    }
    &__textarea {
      ${tw`lg:mb-[64px]`}
    }
  }
  
  .modal-button {
    ${tw`p-[0] mb-[40px] lg:mb-[0px] w-[287px] lg:w-[310px] lg:mr-[32px] h-[64px] rounded-none text-[16px] text-white hover:scale-105 hover:text-white `}
    background-color: #659DBD;

    &:hover {
      background-color: #5B8DAA;
    }
  } 
  .modal-button-default {
    ${tw`mb-[24px] lg:mb-[0px] w-[287px] h-[64px] rounded-none text-[16px] hover:scale-105 lg:mr-[32px]`}
    background-color: transparent;
    border: 1px solid #EAEAEA;
    color: rgba(35, 35, 35, .7);
    &__question {width: 281px;}
    &__complaints {width: 200px;}
    &__other {width: 166px; margin-bottom: 40px;}
    &__send {margin-bottom: 0;}

    &:hover {
      background-color: #5B8DAA;
    }
  }
  .modal-teaser-checkbox {
    ${tw`lg:flex lg:w-[100%] lg:justify-between lg:items-center`}
    margin-bottom: 40px;
  }
`
