import React from 'react';
import {ToursStyles} from './Tours.styles.tw';
import 'swiper/css'
import {Swiper, SwiperSlide} from 'swiper/react'
import tours1 from '../../assets/img/Photo 1.jpg';
import tours2 from '../../assets/img/Photo 2.jpg';
import tours3 from '../../assets/img/Photo 3.jpg';
import {FreeMode, Navigation} from 'swiper';
import { ReactComponent as ToursNext } from '../../assets/svg/arrow-next.svg';
import { ReactComponent as ToursPrev } from '../../assets/svg/arrow-prev.svg';


function Tours() {
  return (
    <ToursStyles className="tours">
      <h1>Other tours</h1>
      <div className="tours-swiper">
        <Swiper
          className={'tours-list'}
          modules={[FreeMode, Navigation]}
          freeMode={{enabled: true}}
          loop={true}
          navigation={{
            nextEl: '.tours-button-next',
            prevEl: '.tours-button-prev'
          }}
          breakpoints={{
            375: {
              spaceBetween: 24,
              slidesPerView: 1.2,
            },
            1250: {
              spaceBetween: 33,
              slidesPerView: 4,
            },
          }}
        >
          <SwiperSlide className={'tours-list-item'}>
            <img className="tours-list-item__image" src={tours1} alt="g1"/>
            <p className="tours-list-item__title">St. Isaac's Cathedral in St. Petersburg</p>
            <span className="tours-list-item__description">One of the highest domed structures in the world.</span>
          </SwiperSlide>
          <SwiperSlide className={'tours-list-item'}>
            <img className="tours-list-item__image"  src={tours2} alt="g2"/>
            <p className="tours-list-item__title">The Bridge of Peace, Tbilisi</p>
            <span className="tours-list-item__description">Arc-shaped pedestrian bridge made of glass and steel.</span>
          </SwiperSlide>
          <SwiperSlide className={'tours-list-item'}>
            <img className="tours-list-item__image"  src={tours3} alt="g3"/>
            <p className="tours-list-item__title">El Caminito del Rey, Argentina</p>
            <span className="tours-list-item__description">Tango, Open-air Museum, Painting, Art and History.</span>
          </SwiperSlide>
        </Swiper>
        <div className="tours-button-prev">
          <ToursPrev className="arrow"/>
        </div>
        <div className="tours-button-next">
          <ToursNext className="arrow"/>
        </div>
      </div>
    </ToursStyles>
  );
}

export default Tours;
