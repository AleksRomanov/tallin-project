import React, {useEffect, useState} from 'react';
import {TeaserStyles} from './Teaser.styles.tw';
import {Button, Modal} from 'antd';
import ModalTeaser from './ModalTeaser/ModalTeaser';
import "../../styles/index.scss";
import useWindowSize from '../../lib/hooks/window-resize';

function Teaser() {
  const windowSize = useWindowSize();
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [modalWidth, setModalWidth] = useState(0);
  useEffect(() => {
    if (windowSize.width) {
      setModalWidth(windowSize.width)
    }
  }, [windowSize])

  const showModal = () => {
    setIsModalOpen(true);
  };

  const handleOk = () => {
    setIsModalOpen(false);
  };

  const handleCancel = () => {
    setIsModalOpen(false);
  };
  return (
    <TeaserStyles className="teaser">
      <h1>Walking in Old Tallinn</h1>
      <div className="teaser-content">
        <p>Welcome to Old Tallinn, the heart of the Estonian capital! The history of the settlement once began from here, and today it is one of the best preserved medieval cities in Europe.</p><br/>
        <p>I will take you through the streets and doorways and show you where they traded, what they ate, what they sued about and what our Estonian ancestors aspired to.</p><br/>
        <p>You will visit the oldest pharmacy, get acquainted with the royal privileges, see firsthand a real "gingerbread" house, look into an old tavern, walk along the widest and longest streets of the city and even find out that Tallinn is a lame city!</p><br/>
        <p>And you will also find beautiful and interesting souvenirs, of course!</p>
      </div>
      <Button type="primary" onClick={showModal} className="teaser-feedback">Feedback</Button>
      <Modal maskStyle={{padding: "20px"}} style={{maxWidth: '100%'}} width={modalWidth} className="teaser-modal" open={isModalOpen} onOk={handleOk} onCancel={handleCancel}>
      {/*<Modal maskStyle={{padding: "20px"}} style={{maxWidth: '100%'}} width={modalWidth} className="teaser-modal" open={isModalOpen} onOk={handleOk} onCancel={handleCancel}>*/}
        <ModalTeaser />
      </Modal>
    </TeaserStyles>
  );
}

export default Teaser;
