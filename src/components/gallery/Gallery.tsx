import React from 'react';
import {GalleryStyles} from './Gallery.styles.tw';
import 'swiper/css'
import {Swiper, SwiperSlide} from 'swiper/react'
import gallery1 from '../../assets/img/gallery-swiper/info1.jpg';
import gallery2 from '../../assets/img/gallery-swiper/info2.jpg';
import gallery3 from '../../assets/img/gallery-swiper/info3.jpg';
import gallery4 from '../../assets/img/gallery-swiper/info4.jpg';
import {ReactComponent as GalleryNext} from '../../assets/svg/arrow-next.svg';
import {ReactComponent as GalleryPrev} from '../../assets/svg/arrow-prev.svg';
import {FreeMode, Navigation} from 'swiper';


function Gallery() {
  return (
    <GalleryStyles className="swiper gallery">
      <div className="swiper-wrapper gallery-swiper">
        <Swiper
          className={'gallery-list'}
          modules={[FreeMode, Navigation]}
          freeMode={{enabled: true}}
          loop={true}
          navigation={{
            nextEl: '.gallery-button-next',
            prevEl: '.gallery-button-prev'
          }}
          breakpoints={{
            375: {
              spaceBetween: 24,
              slidesPerView: 1.2,
            },
            1250: {
              spaceBetween: 32,
              slidesPerView: 3.9,
            },
          }}
        >
          <SwiperSlide className={'swiper-slide gallery-list-item'}>
            <img src={gallery1} alt="g1"/>
          </SwiperSlide>
          <SwiperSlide className={'swiper-slide gallery-list-item'}>
            <img src={gallery2} alt="g2"/>
          </SwiperSlide>
          <SwiperSlide className={'swiper-slide gallery-list-item'}>
            <img src={gallery3} alt="g3"/>
          </SwiperSlide>
          <SwiperSlide className={'swiper-slide gallery-list-item'}>
            <img src={gallery4} alt="g4"/>
          </SwiperSlide>
        </Swiper>
        <div className="gallery-nav">
          <div className="gallery-button-prev">
            <GalleryPrev className="arrow"/>
          </div>
          <div className="gallery-button-next">
            <GalleryNext className="arrow"/>
          </div>
        </div>
      </div>
    </GalleryStyles>
  );
}

export default Gallery;
