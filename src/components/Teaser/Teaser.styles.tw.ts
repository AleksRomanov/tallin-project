import tw from 'twin.macro';
import styled from 'styled-components';

export const TeaserStyles = styled.div`
  ${tw`relative  flex justify-center items-center flex-wrap lg:block lg:items-start lg:justify-between px-[20px] pt-[40px] text-black mb-[64px] lg:px-[128px]`}
  h1 {
    ${tw`mr-[auto] lg:mr-[0] text-[24px] mb-[40px] lg:text-[40px] lg:mb-[73px] lg:order-1`}
  }
  .teaser-feedback {
    ${tw`p-[0] order-1 lg:order-1  mb-[40px] w-[334px] h-[64px] rounded-none text-[16px] text-white hover:scale-105 hover:text-white lg:absolute lg:right-[130px] lg:top-[40px]`}
    background-color: #659DBD;
    &:hover {
      background-color: #5B8DAA;
    }
  }
  .teaser-content {
    ${tw`relative order-2 lg:order-3 lg:w-[586px]`}
  }
  p {
    ${tw`lg:w-[586px]`}
  }
  .teaser-modal {
    ${tw`rounded-none  `}
    
  }

`
