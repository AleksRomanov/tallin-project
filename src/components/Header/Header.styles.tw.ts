import tw from 'twin.macro';
import styled from 'styled-components';

export const HeaderStyles = styled.div`
  ${tw`flex items-center justify-center text-black text-base  2xl:h-[480px]`}
  .header-background {
    ${tw`w-[]`}
  }
`
